﻿using _001_Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_EntityFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ORM_Example1Entities db = new ORM_Example1Entities()) // DB ye kayıt gönderilmesi için Context üzerinden bir instance(nesne) türetilir.
            {
                List<Department> departments = new List<Department>();

                #region EntityFramework ile DB olmadan sadece Listeye atılan değerler
                
                //I.Yol
                Department department1 = new Department(); // Department sınıfından nesne türetilir.
                department1.Name = "Computer Engineering"; // Department sınıfına ait Name propertysi (DB'deki karşılığı sütundur) set edilir. 
                departments.Add(department1);              // List<Department> tipinde oluşturulmuş listeye yeni oluşturulmuş nesne eklenir.
                                                           // 
                //II.Yol
                departments.Add(new Department { Name = "Software Engineering" }); // Nesne eklemenin farklı ve daha basit bir yolu.

                //Oluşturulan bölümlerin isimleri listelenir
                var showList = departments.ToList(); // Bu kısım gereksiz olarak görülebilir.(Şuan departments zaten bir List<Department> tipinde listedir. )
                foreach (var department in showList) // Direkt olarak departments listesinden döngü döndürülebilirdi.
                {
                    Console.WriteLine(department.Name); // Console üzerine tek tek eklenen bölümler yazılır.
                }

                // Bu kısımda sadece EntityFramework ile nasıl veri eklenir o anlatılmıştır.
                // DB ile herhangi bir şekilde iletişim kurulup kayıt eklenmemiştir!
                #endregion

                #region EntityFramework ile generate edilen DB'ye SQL işlemleri (CRUD)
                //DB'ye kayıt işlemleri
                try
                {
                    // EntityFramework ile listeye eklenen kayıtlar aynı şekilde bu sefer DB üzerinde bulunan tablolara eklenir.
                    // Yine aynı şekilde iki farklı yol ile ekleme yapılabilinir.
                    db.Department.Add(department1);
                    db.Department.Add(new Department { Name = "SoftwareEngineering" });

                    // Eğer aşagıdaki method çalıştırılmazsa DB ye herhangi bir işlem gerçekleşmez.
                    // CRUD işlemelerinden Create Update ve Delete işlemlerinde kesinlikle SaveChanges() methodu çalıştırılmalıdır.
                    // Debug modunda bekleyerek DB de kontrol edebilirsiniz.
                    db.SaveChanges();

                    Console.WriteLine("Veriler DB'ye başarılı bie şekilde kaydedildi :)");
                    Console.ReadKey();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Veriler DB'ye kaydedilirken hata oluştu!");
                    Console.WriteLine(ex.Message);
                    Console.ReadKey();
                }
                #endregion

            }
        }
    }
}
